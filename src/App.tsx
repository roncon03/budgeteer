import React, { Component } from 'react'
import { MainComponent } from './components/main/main-component'
import { BudgetCalculatorService } from './services/budget-calculator-service/budget-calculator-service';
import { ServiceContainer } from './utils/service-container';

import './App.css'

/**
 * Unique ID for each screen.
 */
export enum ScreenId {
  Main,
  CreateChart,
  ShowChart
}

/**
 * Interface for components to talk to
 * the main application.
 */
export interface AppInterface {

  /**
   * Shows the given screen.
   * @param screenId The ID of the screen to display.
   */
  setScreenId(screenId: ScreenId): void

  /**
   * Shows the given screen and passes properties to it.
   * @param screenId The ID of the screen to display.
   * @param sharedProps The properties to the pass to the component.
   */
  setScreenIdWithProps(screenId: ScreenId, sharedProps: {}): void

  /**
   * Retrieves the ID of the current screen being shown.
   */
  getScreenId(): ScreenId

  /**
   * Retrieves the shared properties.
   */
  getSharedProps(): {}
}

/**
 * Generic component props wrapper.
 */
export interface ComponentProps {
  app: AppInterface
}

/**
 * Wraps the state object.
 */
class AppState {
  currentScreenId: number = 0
  sharedProps: {} = {}
}

class App extends Component implements AppInterface {
  private appState: AppState
  private serviceContainer: ServiceContainer

  constructor(props: any) {
    super(props)
    this.appState = new AppState()
    this.serviceContainer = new ServiceContainer()
  }

  setScreenId(screenId: ScreenId) {
    this.appState.currentScreenId = screenId
    this.setState(this.appState)
  }

  setScreenIdWithProps(screenId: ScreenId, sharedProps: {}) {
    this.appState.sharedProps = sharedProps
    this.setScreenId(screenId)
  }

  getScreenId() {
    return this.appState.currentScreenId
  }

  getSharedProps() {
    return this.appState.sharedProps
  }

  render() {
    return (
      <div className="App">
        <MainComponent budgetCalculatorService={this.serviceContainer.getService(BudgetCalculatorService)} />
      </div>
    )
  }
}

export default App
