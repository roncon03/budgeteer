import React, { Component, ChangeEvent } from 'react';
import { ComponentProps, AppInterface, ScreenId } from '../../App'
import { FinanceData } from '../../models/finance-data';
import { Income } from '../../models/income';
import { Tax } from '../../models/tax';
import { Expense } from '../../models/expense';
import { BudgetCalculatorServiceInterface } from '../../services/budget-calculator-service/budget-calculator-service';
import { ShowChartComponent, ShowChartComponentListener } from '../show-chart/show-chart-component';
import { Budget } from '../../models/budget';
import FileSaver from 'file-saver'

interface CreateChartComponentProps extends ComponentProps {
    budgetCalculatorService: BudgetCalculatorServiceInterface
}

interface CreateChartComponentState {
    value: number,
    income: number,
    incomeTax: number,
    medicareTax: number,
    socialSecurityTax: number,
    rentPayment: number,
    carPayment: number,
    showChartComponentVisible: boolean
}

export class CreateChartComponent extends Component<CreateChartComponentProps, {}> implements ShowChartComponentListener {

    private budgetCalculatorService: BudgetCalculatorServiceInterface
    private budgets: Budget[]

    constructor(props: CreateChartComponentProps) {
        super(props)

        this.initState()
        this.budgetCalculatorService = props.budgetCalculatorService
        this.budgets = []
    }

    /**
     * Initializes the component's state.
     */
    initState() {
        let state = {} as CreateChartComponentState
        state.income = 0
        state.incomeTax = 22
        state.medicareTax = 1.45
        state.socialSecurityTax = 6.2
        state.rentPayment = 0
        state.carPayment = 0
        state.showChartComponentVisible = false

        this.state = state
    }

    /**
     * Updates the component's state.
     * @param state The new state data to be inserted.
     */
    setState(state: CreateChartComponentState) {
        super.setState(state)
    }

    /**
     * Retrieves the component's state.
     * @returns The component's state as a CreateChartComponentState interface.
     */
    getState = () => {
        let state = this.state as CreateChartComponentState
        return state
    }

    /**
     * Called when the income text input is changed.
     * Updates the stored income value.
     */
    onIncomeChange = (event: ChangeEvent<HTMLInputElement>) => {
        let state = this.getState()
        state.income = +event.target.value

        this.setState(state)
    }

    /**
     * Called when the income tax text input is changed.
     * Updates the stored income tax value.
     */
    onIncomeTaxChange = (event: ChangeEvent<HTMLInputElement>) => {
        let state = this.getState()
        state.incomeTax = +event.target.value

        this.setState(state)
    }

    /**
     * Called when the medicare tax text input is changed.
     * Updates the stored medicare tax value.
     */
    onMedicareTaxChange = (event: ChangeEvent<HTMLInputElement>) => {
        let state = this.getState()
        state.medicareTax = +event.target.value

        this.setState(state)
    }

    /**
     * Called when the social security tax text input is changed.
     * Updates the stored social security tax value.
     */
    onSocialSecurityTaxChange = (event: ChangeEvent<HTMLInputElement>) => {
        let state = this.getState()
        state.socialSecurityTax = +event.target.value

        this.setState(state)
    }

    /**
     * Called when the rent payment text input is changed.
     * Updates the stored rent payment value.
     */
    onRentPaymentChange = (event: ChangeEvent<HTMLInputElement>) => {
        let state = this.getState()
        state.rentPayment = +event.target.value

        this.setState(state)
    }

    /**
     * Called when the car payment text input is changed.
     * Updates the stored car payment value.
     */
    onCarPaymentChange = (event: ChangeEvent<HTMLInputElement>) => {
        let state = this.getState()
        state.carPayment = +event.target.value

        this.setState(state)
    }

    /**
     * Called when the create chart button is clicked.
     * Creates the budget data and goes to the show chart screen.
     */
    onCreateChartButtonClicked = () => {
        let state = this.getState()

        let salaryData = new Income()
        salaryData.name = 'Salary'
        salaryData.value = state.income

        let incomeTaxData = new Tax()
        incomeTaxData.name = 'Federal Income Tax'
        incomeTaxData.value = state.incomeTax / 100

        let medicareTaxData = new Tax()
        medicareTaxData.name = 'Medicare Tax'
        medicareTaxData.value = state.medicareTax / 100

        let socialSecurityTaxData = new Tax()
        socialSecurityTaxData.name = 'Social Security Tax'
        socialSecurityTaxData.value = state.socialSecurityTax / 100

        let rentPayment = new Expense()
        rentPayment.name = 'Rent Payment'
        rentPayment.value = state.rentPayment

        let carPayment = new Expense()
        carPayment.name = 'Car Payment'
        carPayment.value = state.carPayment

        let finances = new FinanceData()
        finances.grossIncomes.push(salaryData)
        finances.taxes.push(incomeTaxData, medicareTaxData, socialSecurityTaxData)
        finances.expenses.push(rentPayment, carPayment)

        let budget = this.budgetCalculatorService.calculate(finances)

        this.budgets = [ budget ]

        state.showChartComponentVisible = true
        this.setState(state)
    }

    /**
     * Called when a file is uploaded to the application.
     * Parses the file data and updates the input fields.
     * @param event Event data with the uploaded file.
     */
    onFileUploaded = (event: ChangeEvent<HTMLInputElement>) => {
        let files = event.target.files

        if (files !== null) {
            for (var i = 0; i < files.length; i++) {
                let file = files.item(i)

                if (file !== null) {
                    let reader = new FileReader()
                    reader.readAsText(file)
                    
                    reader.onload = () => {
                        let jsonData = JSON.parse(reader.result as string)
                        let financeData = this.parseJsonToFinance(jsonData)
                        let state = this.getState()

                        this.updateStateWithFinanceData(state, financeData)
                    }
                }
            }
        }
    }

    /**
     * Parses a JSON object into
     * finances data.
     * 
     * @returns The parsed finance data.
     */
    private parseJsonToFinance = (json: any) => {
        let finances = new FinanceData()

        for (let incomeData of json['incomes']) {
            let income = new Income()
            income.name = incomeData['name']
            income.value = incomeData['value']

            finances.grossIncomes.push(income)
        }

        for (let taxData of json['taxes']) {
            let tax = new Tax()
            tax.name = taxData['name']
            tax.value = taxData['value']

            finances.taxes.push(tax)
        }

        for (let expenseData of json['expenses']) {
            let expense = new Expense()
            expense.name = expenseData['name']
            expense.value = expenseData['value']

            finances.expenses.push(expense)
        }

        return finances
    }

    /**
     * Updates the input fields with
     * the given finance data.
     * @param state The component state binded to the input fields.
     * @param financeData The finance data to update the state with.
     */
    private updateStateWithFinanceData = (state: CreateChartComponentState, financeData: FinanceData) => {
        state.income = financeData.grossIncomes[0].value
        state.incomeTax = financeData.taxes[0].value * 100
        state.medicareTax = financeData.taxes[1].value * 100
        state.socialSecurityTax = financeData.taxes[2].value * 100
        state.rentPayment = financeData.expenses[0].value
        state.carPayment = financeData.expenses[1].value

        this.setState(state)
    }

    /**
     * Called when the save chart button is clicked.
     * Saves the current finance data into a file.
     */
    onSaveChartButtonClicked = () => {
        let state = this.getState()
        let data: any = {
            incomes: [{
                name: 'Salary',
                value: state.income
            }],
            taxes: [{
                name: 'Federal Income Tax',
                value: state.incomeTax / 100
            },
            {
                name: 'Employee Medicare',
                value: state.medicareTax / 100
            },
            {
                name: 'Social Security Employee Tax',
                value: state.socialSecurityTax / 100
            }],
            expenses: [{
                name: 'Rent',
                value: state.rentPayment
            },
            {
                name: 'Car Payment',
                value: state.carPayment
            }]
        }

        var blob = new Blob([JSON.stringify(data)]);
        FileSaver.saveAs(blob, `chart-${this.getFormattedDate()}.json`);    
    }

    /**
     * Creates a formatted date-time string
     * from the current date and time.
     */
    private getFormattedDate() {
        let padLength = 2
        let padChar = '0'

        let date = new Date()
        let year = date.getFullYear()
        let month = (date.getMonth() + 1).toString().padStart(padLength, padChar)
        let day = date.getDate().toString().padStart(padLength, padChar)

        return `${year}.${month}.${day}.${date.getTime()}`
    }

    onReturn(): void {
        let state = this.getState()
        state.showChartComponentVisible = false
        this.setState(state)
    }

    render() {
        if (this.getState().showChartComponentVisible === false)
        {
            return (
                <div className="CreateChartComponent">
                    <h1>Create New Chart</h1>
                    <fieldset>
                        <legend>Enter Information:</legend>
                        Income: $<input type="number" min="0" name="income" value={this.getState().income} onChange={this.onIncomeChange} /><br />
                        Federal Income Tax: <input type="number" min="0" name="fedincometax" value={this.getState().incomeTax} onChange={this.onIncomeTaxChange} />%<br />
                        Employee Medicare: <input type="number" min="0" name="employeemedicare" value={this.getState().medicareTax} onChange={this.onMedicareTaxChange} />%<br />
                        Social Security Employee Tax: <input type="number" min="0" name="socialsecuritytax" value={this.getState().socialSecurityTax} onChange={this.onSocialSecurityTaxChange} />%<br />
                        Car Payment: $<input type="number" min="0" name="carpayment" value={this.getState().carPayment} onChange={this.onCarPaymentChange} /><br />
                        Rent: $<input type="number" min="0" name="rentpayment" value={this.getState().rentPayment} onChange={this.onRentPaymentChange} /><br />
                        <button onClick={this.onCreateChartButtonClicked}>Create</button>
                        <button onClick={this.onSaveChartButtonClicked}>Save Chart</button>
                        <br />
                        <input type="file" id="input" accept="application/json" onChange={this.onFileUploaded}></input>
                    </fieldset>
                </div>
            )
        }

        return(
            <ShowChartComponent budgets={this.budgets} listener={this}/>
        )
    }
}