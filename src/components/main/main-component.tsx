import React, { Component } from 'react';
import { AppInterface, ComponentProps, ScreenId } from '../../App'
import { CreateChartComponent } from '../create-chart/create-chart-component';
import { BudgetCalculatorServiceInterface } from '../../services/budget-calculator-service/budget-calculator-service';

export interface MainComponentProps {
    budgetCalculatorService: BudgetCalculatorServiceInterface
}

export interface MainComponentState {
    showCreateChartComponent: boolean
}

export class MainComponent extends Component<MainComponentProps> {
    private budgetCalculatorService: BudgetCalculatorServiceInterface

    constructor(props: MainComponentProps) {
        super(props)
        this.budgetCalculatorService = props.budgetCalculatorService
        this.initState()
    }

    /**
     * Initializes the component state.
     */
    private initState() {
        this.state = {}
        let state = this.getState()
        state.showCreateChartComponent = false

        this.setState(state)
    }

    /**
     * Called when the create graph button is clicked.
     */
    private onShowCreateGraph = () => {
        let state = this.getState()
        state.showCreateChartComponent = true

        this.setState(state)
    }

    private getState() {
        return this.state as MainComponentState
    }

    render() {
        if (this.getState().showCreateChartComponent === false) {
            return (
                <div className="MainComponent">
                    <h1>Budgeteer</h1>
                    <button onClick={this.onShowCreateGraph}>Create Graph</button>
                </div>
            )
        }

        return <CreateChartComponent app={{} as AppInterface} budgetCalculatorService={this.budgetCalculatorService}/>
    }
}