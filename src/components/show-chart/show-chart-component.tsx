import React, { Component } from 'react'
import { PieChart, Pie, Cell, ResponsiveContainer, PieLabelRenderProps } from 'recharts'
import { AppInterface, ComponentProps, ScreenId } from '../../App'
import { Budget } from '../../models/budget';
import { FinanceType } from '../../models/finance';

export interface ShowChartComponentListener {
    /**
     * Called when the user wants to go back
     * to the previous component view.
     */
    onReturn(): void
}

export interface ShowChartComponentProps {
    budgets: Budget[]
    listener: ShowChartComponentListener
}

export class ShowChartComponent extends Component<ShowChartComponentProps, {}> {
    private static readonly defaultColor = '000000'
    private static readonly incomeColor = '17e324'
    private static readonly taxColor = 'a6111b'
    private static readonly expenseColor = 'ff0000'

    private budgets: Budget[]
    private listener: ShowChartComponentListener
    private data: any[]

    constructor(props: ShowChartComponentProps) {
        super(props)
        
        this.budgets = props.budgets
        this.listener = props.listener
        this.state = { budgets: this.budgets }
        this.data = this.buildData(this.budgets[0])
    }

    /**
     * Builds the chart data.
     * @param budget The budget to build the data from.
     * @returns The build data array for the chart.
     */
    buildData = (budget: Budget) => {
        let data = []

        if (budget.netIncome.value > 0)
        {
            let netIncomeData = {
                name: budget.netIncome.name,
                value: budget.netIncome.value,
                type: FinanceType.Income
            }
            data.push(netIncomeData)
        }

        for (let tax of budget.taxes) {
            if (tax.value > 0)
            {
                let taxData = {
                    name: tax.name,
                    value: tax.value,
                    type: FinanceType.Tax
                }

                data.push(taxData)
            }
        }

        for (let expense of budget.expenses) {
            if (expense.value > 0)
            {
                let expenseData = {
                    name: expense.name,
                    value: expense.value,
                    type: FinanceType.Expense
                }

                data.push(expenseData)
            }
        }

        return data
    }

    /**
     * Renders a custom label for the chart.
     * @param props The pie chart label render properties.
     */
    renderCustomizedLabel = (props: PieLabelRenderProps) => {
        let innerRadius = props.innerRadius as number
        let outerRadius = props.outerRadius as number
        let cx = props.cx as number
        let cy = props.cy as number
        let midAngle = props.midAngle as number

        let RADIAN = Math.PI / 180; 
        let radius = innerRadius + (outerRadius - innerRadius) * 1.6;
        let x  = cx + radius * Math.cos(-midAngle * RADIAN);
        let y = cy  + radius * Math.sin(-midAngle * RADIAN);
    
        return (
            <text x={x} y={y} fill="black" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
                {/* <tspan x={cx} y={cy}>{`${this.budgets[0].totalGrossIncome.value.toFixed(2)}`}</tspan> */}
                <tspan>{`${props.name}: $${props.value.toFixed(2)}`}</tspan>
            </text>
        );
    };

    /**
     * Renders the cell color for each segment of the pie chart.
     * @param data The data to be rendered in the pie chart.
     */
    renderPieChartSegmentColor = (data: any[]) => {
        return data.map((entry: {name: string, value: number, type: number}) => {
            var color = ShowChartComponent.defaultColor
            
            if (entry.type === FinanceType.Income) {
                color = ShowChartComponent.incomeColor
            }

            else if (entry.type === FinanceType.Tax) {
                color = ShowChartComponent.taxColor
            }

            else if (entry.type === FinanceType.Expense) {
                color = ShowChartComponent.expenseColor
            }

            return <Cell fill={`#${color}`}/>
        })
    }

    /**
     * Called when the return button is clicked.
     * Tells the app to go back to the create chart screen.
     */
    onReturnButtonClicked = () => {
        this.listener.onReturn()
    }

    render() {
        return (
            <div className="ShowChartComponent">
                <h1>Budget</h1>
                <ResponsiveContainer width="100%" height={250}>
                <PieChart>
                    <Pie data={this.data} 
                    dataKey="value" 
                    nameKey="name" 
                    cx="50%" 
                    cy="50%" 
                    innerRadius={40} 
                    outerRadius={80} 
                    fill="#82ca9d" 
                    label={this.renderCustomizedLabel}>
                    {this.renderPieChartSegmentColor(this.data)}
                    </Pie>
                </PieChart>
                </ResponsiveContainer>
                <button onClick={this.onReturnButtonClicked}>Go Back</button>
            </div>
        );
    }
}
  