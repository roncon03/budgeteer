import { Income } from "./income";
import { Tax } from "./tax";
import { Expense } from "./expense";

export class Budget {
    grossIncomes: Income[]
    taxes: Tax[]
    expenses: Expense[]
    totalGrossIncome: Income
    netIncome: Income

    constructor() {
        this.grossIncomes = []
        this.taxes = []
        this.expenses = []
        this.totalGrossIncome = new Income()
        this.netIncome = new Income()
    }
}