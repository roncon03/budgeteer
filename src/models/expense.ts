import { Finance, FinanceType } from "./finance";

export class Expense extends Finance {
    constructor() {
        super()
        this.type = FinanceType.Expense
    }
}