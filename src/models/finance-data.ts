import { Income } from "./income";
import { Tax } from "./tax";
import { Expense } from "./expense";

export class FinanceData {
    grossIncomes: Income[] = []
    taxes: Tax[] = []
    expenses: Expense[] = []
}