export enum FinanceType {
    Income,
    Tax,
    Expense
}

export class Finance {
    protected type = FinanceType.Income

    private _value: number
    get value(): number {
        return this._value
    }
    set value(value: number) {
        if (value < 0) {
            value = 0
        }

        this._value = value
    }

    name: string
    
    constructor() {
        this.name = ""
        this._value = 0
        this.type = FinanceType.Income
    }
}