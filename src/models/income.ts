import { Finance, FinanceType } from "./finance";

export class Income extends Finance {
    constructor() {
        super()
        this.type = FinanceType.Income
    }
}