import { Finance, FinanceType } from "./finance";

export class Tax extends Finance {
    constructor() {
        super()
        this.type = FinanceType.Tax
    }
}