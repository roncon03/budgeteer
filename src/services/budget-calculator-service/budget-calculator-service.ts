import { FinanceData } from "../../models/finance-data";
import { Budget } from "../../models/budget";
import { Income } from "../../models/income";
import { Tax } from "../../models/tax";
import { Expense } from "../../models/expense";
import { ServiceInterface } from "../service-interface";

export interface BudgetCalculatorServiceInterface {
    calculate(finances: FinanceData): Budget

    calculateTotalGrossIncome(incomes: Income[]): number

    calculateTaxes(income: number, taxes: Tax[]): Tax[]

    calculateNetIncome(income: number, taxes: Tax[], expenses: Expense[]): number
}

export class BudgetCalculatorService implements BudgetCalculatorServiceInterface, ServiceInterface
{
    name = BudgetCalculatorService.name;

    calculate(finances: FinanceData) {
        let budget = new Budget()
        budget.grossIncomes = finances.grossIncomes
        budget.expenses = finances.expenses

        budget.totalGrossIncome.name = "Gross Income"
        budget.totalGrossIncome.value = this.calculateTotalGrossIncome(finances.grossIncomes)

        budget.taxes = this.calculateTaxes(budget.totalGrossIncome.value, finances.taxes)
        
        budget.netIncome.name = "Net Income"
        budget.netIncome.value = this.calculateNetIncome(budget.totalGrossIncome.value, budget.taxes, budget.expenses)

        return budget
    }
    
    calculateTotalGrossIncome(incomes: Income[]) {
        let totalGrossIncome = 0

        for (let income of incomes) {
            totalGrossIncome += income.value
        }

        return totalGrossIncome
    }

    calculateTaxes(income: number, taxes: Tax[]) {
        let calculatedTaxes: Tax[] = []

        for (let tax of taxes) {
            let calculatedTax = new Tax()
            calculatedTax.name = tax.name
            calculatedTax.value = income * tax.value

            calculatedTaxes.push(calculatedTax)
        }

        return calculatedTaxes
    }

    calculateNetIncome(income: number, taxes: Tax[], expenses: Expense[]) {
        for (let tax of taxes) {
            income -= tax.value
        }

        for (let expense of expenses) {
            income -= expense.value
        }

        return income
    }
}