/**
 * Common service interface for container registration.
 */
export interface ServiceInterface {
    /**
     * Name of the service for registration.
     */
    readonly name: string
}