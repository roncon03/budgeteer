import { BudgetCalculatorService } from "../services/budget-calculator-service/budget-calculator-service";
import { ServiceInterface } from "../services/service-interface";

/**
 * Service container used to register services.
 * Used for dependency injection.
 */
export class ServiceContainer {
    private services: {[key: string]: any}

    constructor() {
        this.services = {}
        this.initializeContainer()
    }

    /**
     * Initializes the container with pre-registered services.
     * New services should be registered in this method.
     */
    private initializeContainer() {
        this.register(new BudgetCalculatorService())
    }

    /**
     * Registers a new service using its class name
     * as the unique ID. If a service is re-registered,
     * the previous service will be overwritten.
     * @param service The service to be registered.
     */
    register<ServiceImplementation extends ServiceInterface>(service: ServiceImplementation) {
        this.services[service.name] = service
    }

    /**
     * Retrieves the given service.
     * @param service The service class to retrieve.
     */
    getService<ServiceImplementation>(service: {new(): ServiceImplementation;}) {
        return this.services[service.name]
    }
}